def RepresentsInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

def tableToDictionary(archive):
	a = open(archive, 'r')
	text = a.readlines()
	a.close()

	table = {}

	linha = 0
	while linha < len(text):
		if (text[linha] == "\n") or (text[linha][0] == ";") : # linha sem informação 
			indexLine = text.index(text[linha])
			text.pop(indexLine)
		else:
			text[linha] = text[linha].strip().split(' ')
			noTab = text[linha][0].split('\t')
			table[noTab[0].strip()] = linha
			linha +=1

	return table

def progArchiveToVector(programFile):
	f = open(programFile, 'r')
	texto = f.readlines()
	f.close()

	linha = 0

	#Tokeniza a linha armazena em texto[i]
	while linha < len(texto):
		if texto[linha] == "\n": # linha sem inforação
			local = texto.index(texto[linha])
			texto.pop(local)
		else:
			texto[linha] = texto[linha].strip().split(' ')
			linha += 1

	#Retira comentários e armazena em texto[i]
	for l in texto: # cada linha
		indexLinha = texto.index(l) # a linha
		for b in l: # cada token da linha
			indexVector = texto[indexLinha].index(b) # indice da linha sendo avaliado
			if (";" in b):
				rangeComment = len(texto[indexLinha]) - indexVector
				texto[indexLinha] = texto[indexLinha][:-rangeComment]
				break

	#Tokeniza operandos e armazena em texto[i]
	for l in texto: # cada linha
		indexLinha = texto.index(l) # a linha
		for b in l: # cada token da linha
			if "," in b:
				operandos = texto[indexLinha][1].split(',')
				operador = texto[indexLinha][0]
				texto[indexLinha] = [operador,operandos[0],operandos[1]]

	return texto

def comandsToCode(commands, opCodes, regCodes):
	twoParameters = {"MOV_RR","MOV_RM","MOV_MR","MOV_RI","MOV_MI","ADD","MUL","DIV","SUB","CMP"}
	unaryParameters = {"JZ","JMP","JG","JL","OUT","INC","DEC"}
	
	for l in commands: # cada linha
		indexLinha = commands.index(l) # a linha
		if commands[indexLinha][0] == "MOV":
			if "[" in commands[indexLinha][1]: # ANALISA O PRIMEIRO OPERANDO
				commands[indexLinha][0] = "MOV_M"
			else:
				commands[indexLinha][0] = "MOV_R"

			if RepresentsInt(commands[indexLinha][2]): # ANALISA O SEGUNDO OPERANDO
				commands[indexLinha][0] += "I"
			else: 
				if "[" in commands[indexLinha][2]:
					commands[indexLinha][0] += "M"
				else:
					commands[indexLinha][0] += "R"

	prog = []
	for l in commands: # cada linha
		indexLinha = commands.index(l) # a linha
		if commands[indexLinha][0] in opCodes:
			prog.append(opCodes.get(commands[indexLinha][0]))
		
		if commands[indexLinha][0] in twoParameters:
			#Primeiro operando
			if "[" in commands[indexLinha][1]:
				aux = commands[indexLinha][1].replace("[","")
				aux = aux.replace("]","")
				prog.append(int(aux))
			else:
				prog.append(regCodes.get(commands[indexLinha][1]))

			#Segundo operando
			if "[" in commands[indexLinha][2]:
				aux = commands[indexLinha][2].replace("[","")
				aux = aux.replace("]","")
				prog.append(int(aux))
			else:
				if RepresentsInt(commands[indexLinha][2]):
					prog.append(int(commands[indexLinha][2]))
				else:
					prog.append(regCodes.get(commands[indexLinha][2]))
		else:
			if commands[indexLinha][0] in unaryParameters:
				if RepresentsInt(commands[indexLinha][1]):
					prog.append(int(commands[indexLinha][1]))
				else:
					prog.append(regCodes.get(commands[indexLinha][1]))
				
	return prog 

def assembler(programFile):
	opCodes = tableToDictionary("OPCODES.esym")
	regCodes = tableToDictionary("REGCODES.esym")
	vectorOfLinesWithCommands = progArchiveToVector(programFile)
	codes = comandsToCode(vectorOfLinesWithCommands,opCodes,regCodes)
	
	nameFile = programFile.split('.')
	f = open(nameFile[0]+".run", 'w+')
	
	f.write(' '.join(str(e) for e in codes))
	f.close()
	
	return codes

assembler("prog.easm")
